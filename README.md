## Laravel 5 Settings package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 5.5+
* [Composer](https://getcomposer.org/)

#### Installation

* add to your Laravel app composer.json

```
"require": {
    "smorken/settings": "~5.0"
}
```

* `composer update`

* add service provider to `config/app.php`

```
'providers' => [
...
    \Smorken\Settings\ServiceProvider::class,
```

* publish any needed files

```
$ php artisan vendor:publish --provider="Smorken\Settings\ServiceProvider" --tag=db #view and config also available
```

* run the migrations (might need to dump-autoload again)

```
$ php artisan migrate
```

#### Use

`app('settings')` provides an instance of `Smorken\Settings\Contracts\Storage\Setting`

`setting()` is a shortcut for the same

Get a setting value:

```
$s = app('settings');
$value = $s->get('foo.key');

$value = setting('foo.key', 'default_value');
```

Set a settings value (shortcut):

```
$s = app('settings');
$s->set('foo.key', 'value');

setting()->set('foo.key', 'value');
```
