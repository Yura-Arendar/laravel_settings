<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/8/14
 * Time: 7:27 AM
 */
return [
    'master' => 'smorken/views::layouts.rightops',
    'settings'         => [
        'name'  => 'Smorken\Settings\Contracts\Storage\Setting',
        'impl'  => \Smorken\Settings\Storage\Eloquent\Setting::class,
        'model' => \Smorken\Settings\Models\Eloquent\Setting::class,
    ],
];
