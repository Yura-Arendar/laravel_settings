<?php
$factory->define(
    Smorken\Settings\Models\Eloquent\Setting::class,
    function (Faker\Generator $faker) {
        return [
            'key'   => str_random(10),
            'descr' => $faker->words(3, true),
            'value' => $faker->words(10, true),
        ];
    }
);
