<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTables extends Migration
{

    protected $tables = [
        'settings',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->settings();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->tables as $table) {
            Schema::drop($table);
        }
    }

    protected function settings()
    {
        Schema::create(
            'settings',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('key', 32);
                $table->string('descr', 64);
                $table->string('value', 255);
                $table->timestamps();

                $table->unique('key', 'settings_key_ndx');
            }
        );
    }
}
