<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:54 AM
 */

namespace Smorken\Settings\Contracts\Models;

/**
 * Interface Setting
 * @package Smorken\Settings\Contracts\Models
 *
 * @property integer $id
 * @property string $key
 * @property string $descr
 * @property mixed $value
 */
interface Setting
{

}
