<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:55 AM
 */

namespace Smorken\Settings\Contracts\Storage;

interface Setting
{

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * @param string $key
     * @param mixed $value
     * @return null
     */
    public function set($key, $value);
}
