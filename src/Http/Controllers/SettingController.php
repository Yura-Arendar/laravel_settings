<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:03 AM
 */

namespace Smorken\Settings\Http\Controllers;

use Smorken\ControllerTraited\TraitedController;
use Smorken\Settings\Contracts\Storage\Setting;
use Smorken\Settings\Http\Requests\SettingForm;

class SettingController extends TraitedController
{

    protected $base = 'setting';

    protected $subnav = 'admin';

    protected $package = 'smorken/settings::';

    public function __construct(Setting $provider)
    {
        $this->setProvider($provider);
        parent::__construct();
    }

    public function postSave(SettingForm $r, $id = null)
    {
        $data = $r->all();
        if ($id) {
            $m = $this->loadModel($id);
            $data = array_replace($m->getAttributes(), $data);
            $result = $this->postUpdateDefault($id, $data);
        } else {
            $result = $this->postCreateDefault($data);
        }
        \Cache::flush();
        return $result;
    }

    /**
     * Setup the master template.
     *
     * @return void
     */
    protected function setupMaster()
    {
        if (!$this->master) {
            $this->master = config('smorken/settings::config.master', 'smorken/views::layouts.rightops');
        }
        if ($this->master) {
            view()->share('master', $this->master);
        }
    }
}
