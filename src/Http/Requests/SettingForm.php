<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/20/16
 * Time: 9:00 AM
 */

namespace Smorken\Settings\Http\Requests;

use Smorken\Ext\Http\Requests\Request;
use Smorken\Sanitizer\Contracts\Sanitize;
use Smorken\Settings\Contracts\Storage\Setting;

class SettingForm extends Request
{

    public function rules(Sanitize $s, Setting $provider)
    {
        return $this->sanitizeAndGetRules($s, $provider);
    }

    /**
     * @param array $input
     * @param Sanitize $s
     * @return array
     */
    protected function doSanitize($input, Sanitize $s)
    {
        $input['key'] = $s->string($input['key']);
        $input['descr'] = $s->string($input['descr']);
        $input['value'] = $s->string($input['string']);
        return $input;
    }
}
