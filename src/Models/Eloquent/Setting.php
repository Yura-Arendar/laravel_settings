<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:57 AM
 */

namespace Smorken\Settings\Models\Eloquent;

class Setting extends Base implements \Smorken\Settings\Contracts\Models\Setting
{

    protected $fillable = ['key', 'descr', 'value'];

    protected $rules = [
        'key'   => 'max:32',
        'descr' => 'max:64',
        'value' => 'required',
    ];
}
