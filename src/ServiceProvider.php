<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:04 AM
 */
namespace Smorken\Settings;

use Smorken\Rbac\DocBlock\Parser;
use Smorken\Rbac\Middleware\Rbac;
use Smorken\Storage\Contracts\Binder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'smorken/settings');
        $this->publishes(
            [
                __DIR__ . '/../views' => base_path('/resources/views/vendor/smorken/settings'),
            ],
            'views'
        );
        $this->publishes(
            [
                __DIR__ . '/../database' => base_path('/database'),
            ],
            'db'
        );
        $this->publishes(
            [
                __DIR__ . '/../config' => config_path('/vendor/smorken/settings'),
            ],
            'config'
        );
    }

    public function register()
    {
        $this->registerResources();
        $config = $this->app['config']->get('smorken/settings::config', []);
        $binder = $this->getBinder();
        $this->bindProvider(array_get($config, 'settings', []), $binder);
        $this->app->alias('Smorken\Settings\Contracts\Storage\Setting', 'settings');
    }

    protected function registerResources()
    {
        $userconfigfile = config_path('/vendor/smorken/settings/config.php');
        $packageconfigfile = __DIR__ . '/../config/config.php';
        $this->registerConfig($packageconfigfile, $userconfigfile, 'smorken/settings::config');
    }

    protected function registerConfig($packagefile, $userfile, $namespace)
    {
        $config = $this->app['files']->getRequire($packagefile);
        if (file_exists($userfile)) {
            $userconfig = $this->app['files']->getRequire($userfile);
            $config = array_replace_recursive($config, $userconfig);
        }
        $this->app['config']->set($namespace, $config);
    }

    protected function getBinder()
    {
        if (!$this->app->bound('Smorken\Storage\Contracts\Binder')) {
            $this->app->register(\Smorken\Storage\ServiceProvider::class);
        }
        return $this->app['Smorken\Storage\Contracts\Binder'];
    }

    protected function bindProvider($config, Binder $binder)
    {
        $contract = array_get($config, 'name');
        $impl = array_get($config, 'impl');
        $model = array_get($config, 'model');
        $binder->bindName($contract, $impl, $model);
    }
}
