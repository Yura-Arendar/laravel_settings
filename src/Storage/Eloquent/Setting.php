<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:59 AM
 */

namespace Smorken\Settings\Storage\Eloquent;

class Setting extends Base implements \Smorken\Settings\Contracts\Storage\Setting
{

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        $k = 'settings.' . $key;
        return \Cache::remember(
            $k,
            120,
            function () use ($key, $default) {
                $m = $this->getModel()
                            ->newQuery()
                            ->where('key', '=', $key)
                            ->first();
                if ($m) {
                    return $m->value;
                }
                return $default;
            }
        );
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return null
     */
    public function set($key, $value)
    {
        $data['value'] = $value;
        $m = $this->getModel()
                  ->newQuery()
                  ->where('key', '=', $key)
                  ->first();
        if (!$m) {
            $data = [
                'key'   => $key,
                'descr' => $key,
                'value' => $value,
            ];
            $m = $this->getModel()
                      ->newInstance();
        }
        $m->fill($data);
        return $m->save();
    }
}
