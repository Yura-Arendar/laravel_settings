<?php
if (!function_exists('setting')) {
    function setting($key = null, $default = null)
    {
        $s = app('settings');
        if (!is_null($key)) {
            return $s->get($key, $default);
        }
        return $s;
    }
}
