<?php
namespace Tests\Smorken\Settings\unit\Controllers {

    /**
     * Created by IntelliJ IDEA.
     * User: scoce95461
     * Date: 8/20/16
     * Time: 9:14 AM
     */
    use Mockery as m;

    include_once __DIR__ . '/../helpers.php';
    include_once __DIR__ . '/../CacheHelper.php';

    class RoleControllerTest extends \PHPUnit_Framework_TestCase
    {

        public function tearDown()
        {
            m::close();
        }

        public function testPostSaveWithNoId()
        {
            $input = [
                'key' => 'foo.key',
                'descr' => 'Foo',
                'value' => 'Bar',
            ];
            list($sut, $setting) = $this->getSutAndMocks();
            $s = session();
            $r = redirect();
            $model = m::mock('Setting');
            $sform = m::mock('Smorken\Settings\Http\Requests\SettingForm');
            $sform->shouldReceive('all')->andReturn($input);
            $setting->shouldReceive('create')->with($input)->andReturn($model);
            $setting->shouldReceive('setModel')->with($model);
            $setting->shouldReceive('getModel')->andReturn($model);
            $setting->shouldReceive('errors')->andReturn(false);
            $setting->shouldReceive('name')->with($model)->andReturn('foo');
            $s->shouldReceive('flash')->with('success', 'foo saved.');
            $r->shouldReceive('action')->with(
                '\Smorken\Settings\Http\Controllers\SettingController@getIndex'
            )->andReturn(
                'redirected'
            );
            $this->assertEquals('redirected', $sut->postSave($sform));
        }

        protected function getSutAndMocks()
        {
            $s = m::mock('Smorken\Settings\Contracts\Storage\Setting');
            $v = view();
            $v->shouldReceive('share')->with('master', 'smorken/views::layouts.rightops');
            $v->shouldReceive('share')->with('sub', 'admin');
            $v->shouldReceive('share')->with('controller', 'Smorken\Settings\Http\Controllers\SettingController');
            $sut = new \Smorken\Settings\Http\Controllers\SettingController($s);
            return [$sut, $s];
        }

        public function testPostSaveWithId()
        {
            $input = [
                'value' => 'Bar',
            ];
            list($sut, $setting) = $this->getSutAndMocks();
            $s = session();
            $r = redirect();
            $model = m::mock('Setting');
            $sform = m::mock('Smorken\Settings\Http\Requests\SettingForm');
            $sform->shouldReceive('all')->andReturn($input);
            $setting->shouldReceive('find')->with(1)->andReturn($model);
            $model->shouldReceive('getAttributes')->andReturn(['key' => 'foo.key', 'descr' => 'Foo', 'value' => 'Biz']);
            $setting->shouldReceive('update')->with(
                $model,
                ['key' => 'foo.key', 'descr' => 'Foo', 'value' => 'Bar']
            )->andReturn($model);
            $setting->shouldReceive('setModel')->with($model);
            $setting->shouldReceive('getModel')->andReturn($model);
            $setting->shouldReceive('errors')->andReturn(false);
            $setting->shouldReceive('name')->with($model)->andReturn('foo');
            $s->shouldReceive('flash')->with('success', 'foo saved.');
            $r->shouldReceive('action')->with(
                '\Smorken\Settings\Http\Controllers\SettingController@getIndex'
            )->andReturn(
                'redirected'
            );
            $this->assertEquals('redirected', $sut->postSave($sform, 1));
        }
    }
}

namespace Illuminate\Foundation\Http {

    class FormRequest
    {

    }
}
