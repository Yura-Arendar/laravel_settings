<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 10:12 AM
 */
include_once __DIR__ . '/../../CacheHelper.php';

use Mockery as m;

trait ModelTrait
{

    public function setUp()
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        forward_static_call([$this->getModelClass(), 'unguard']);
    }

    public function tearDown()
    {
        m::close();
        forward_static_call([$this->getModelClass(), 'reguard']);
    }

    protected function getSutAndMocks(array $attrs = [])
    {
        $pdo = m::mock('PDO');
        $connections = [
            'test' => new \Illuminate\Database\SQLiteConnection($pdo),
        ];
        $resolver = new \Illuminate\Database\ConnectionResolver($connections);
        $resolver->setDefaultConnection('test');
        $model_class = $this->getModelClass();
        $sut = new $model_class($attrs);
        forward_static_call([$this->getModelClass(), 'setConnectionResolver'], $resolver);
        return [$sut, $pdo];
    }

    abstract protected function getModelClass();
}



