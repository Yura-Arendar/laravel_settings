<?php
namespace Tests\Smorken\Settings\unit\Models\Eloquent;

use Smorken\Settings\Models\Eloquent\Setting;

include_once __DIR__ . '/ModelTrait.php';

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/17/16
 * Time: 3:08 PM
 */
class SettingModelTest extends \PHPUnit_Framework_TestCase
{

    use \ModelTrait;

    public function testSimple()
    {
        $data = [
            'id'    => 1,
            'key'   => 'foo.key',
            'descr' => 'foo',
            'value' => 'bar',
        ];
        list($sut, $pdo) = $this->getSutAndMocks();
        $pdo->shouldReceive('prepare')->once()->with('select * from "settings" where "key" = ? limit 1')->andReturn(
            $pdo
        );
        $pdo->shouldReceive('execute')->once()->with(['foo.key'])->andReturn($pdo);
        $pdo->shouldReceive('fetchAll')->once()->andReturn(
            [
                $data,
            ]
        );
        $m = $sut->where('key', '=', 'foo.key')->first();
        $this->assertEquals($data, $m->toArray());
    }

    protected function getModelClass()
    {
        return Setting::class;
    }
}
