<?php
namespace Tests\Smorken\Settings\unit;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 2:11 PM
 */
include_once __DIR__ . '/helpers.php';

use Mockery as m;

class ServiceProviderTest extends \PHPUnit_Framework_TestCase
{

    public function testBoot()
    {
        list($sut, $app) = $this->getSutAndMocks();
        $v = $app['view'];
        $v->shouldReceive('addNamespace')->with('smorken/settings', m::any());
        $sut->boot();
    }

    protected function getSutAndMocks()
    {
        $app = m::mock('\Tests\Smorken\Settings\unit\App')->makePartial();
        $app->shouldReceive('basePath')->andReturn(__DIR__);
        $sut = new \Smorken\Settings\ServiceProvider($app);
        return [$sut, $app];
    }

    public function testRegister()
    {
        list($sut, $app) = $this->getSutAndMocks();
        $f = $app['files'];
        $f->shouldReceive('getRequire')->andReturn(true);
        $c = $app['config'];
        $c->shouldReceive('set')->andReturn(true);
        $c->shouldReceive('get')->andReturn('value');
        $b = $app['Smorken\Storage\Contracts\Binder'];
        $b->shouldReceive('bindName')->andReturn(true);
        $app->shouldReceive('bind')->andReturn(true);
        $sut->register();
    }
}

abstract class App implements \Illuminate\Contracts\Foundation\Application, \ArrayAccess
{

    protected $items = [];

    public function alias($abstract, $alias)
    {
        return true;
    }

    public function bound($abstract)
    {
        return true;
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            $m = m::mock($offset);
            $this->offsetSet($offset, $m);
        }
        return array_get($this->items, $offset);
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->items[$offset]);
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        array_set($this->items, $offset, $value);
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }
}
