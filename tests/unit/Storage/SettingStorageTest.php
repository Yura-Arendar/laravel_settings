<?php
namespace Tests\Smorken\Settings\unit\Models;

/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/19/16
 * Time: 12:25 PM
 */
include_once __DIR__ . '/../CacheHelper.php';

use Mockery as m;

class SettingStorageTest extends \PHPUnit_Framework_TestCase
{

    public function tearDown()
    {
        m::close();
    }

    public function testGet()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('newQuery')->once()->andReturn($model);
        $model->shouldReceive('where')->once()->with('key', '=', 'foo.key')->andReturn($model);
        $model->shouldReceive('first')->once()->andReturn($model);
        $model->shouldReceive('getAttribute')->with('value')->andReturn('foo.value');
        $this->assertEquals('foo.value', $sut->get('foo.key'));
    }

    public function testGetWithNoModelReturnsDefault()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('newQuery')->once()->andReturn($model);
        $model->shouldReceive('where')->once()->with('key', '=', 'foo.key')->andReturn($model);
        $model->shouldReceive('first')->once()->andReturn(false);
        $this->assertEquals('default', $sut->get('foo.key', 'default'));
    }

    protected function getSutAndMocks()
    {
        $model = m::mock(\Smorken\Settings\Models\Eloquent\Setting::class);
        $sut = new \Smorken\Settings\Storage\Eloquent\Setting($model);
        return [$sut, $model];
    }

    public function testSetNew()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('newQuery')->once()->andReturn($model);
        $model->shouldReceive('where')->once()->with('key', '=', 'foo.key')->andReturn($model);
        $model->shouldReceive('first')->once()->andReturn(false);
        $model->shouldReceive('newInstance')->once()->andReturn($model);
        $model->shouldReceive('fill')->once()->with(
            ['key' => 'foo.key', 'descr' => 'foo.key', 'value' => 'bar']
        )->andReturn($model);
        $model->shouldReceive('save')->once()->andReturn(true);
        $this->assertTrue($sut->set('foo.key', 'bar'));
    }

    public function testSetExisting()
    {
        list($sut, $model) = $this->getSutAndMocks();
        $model->shouldReceive('newQuery')->once()->andReturn($model);
        $model->shouldReceive('where')->once()->with('key', '=', 'foo.key')->andReturn($model);
        $model->shouldReceive('first')->once()->andReturn($model);
        $model->shouldReceive('fill')->once()->with(['value' => 'bar'])->andReturn($model);
        $model->shouldReceive('save')->once()->andReturn(true);
        $this->assertTrue($sut->set('foo.key', 'bar'));
    }
}
