<?php
use Mockery as m;

$view = null;
$session = null;
$redirect = null;
if (!function_exists('app')) {
    function app($make)
    {
        return m::mock($make);
    }
}
if (!function_exists('view')) {
    function view()
    {
        global $view;
        if (!$view) {
            $view = m::mock('view');
        }
        return $view;
    }
}
if (!function_exists('config')) {
    function config($key = null, $default = null)
    {
        if ($key) {
            return $default;
        }
        return m::mock('config');
    }
}
if (!function_exists('abort')) {
    function abort($code, $message)
    {
        throw new \Exception($message, $code);
    }
}
if (!function_exists('base_path')) {
    function base_path($dir = null)
    {
        return build_path($dir);
    }
}
if (!function_exists('config_path')) {
    function config_path($dir = null)
    {
        return build_path($dir);
    }
}
if (!function_exists('session')) {
    function session()
    {
        global $session;
        if (!$session) {
            $session = m::mock('session');
        }
        return $session;
    }
}
if (!function_exists('redirect')) {
    function redirect()
    {
        global $redirect;
        if (!$redirect) {
            $redirect = m::mock('redirect');
        }
        return $redirect;
    }
}
function build_path($dir)
{
    return __DIR__ . $dir;
}
