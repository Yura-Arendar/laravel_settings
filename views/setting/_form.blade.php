{!! Form::model($model, ['action' => [$controller . '@postSave', $model->id]]) !!}
@if (isset($create))
<div class="form-group">
    {!! Form::label('key', 'Key') !!}
    {!! Form::text('key', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('descr', 'Description') !!}
    {!! Form::text('descr', null, ['class' => 'form-control']) !!}
</div>
@endif
<div class="form-group">
    {!! Form::label('value', 'Value') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>
{!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
{!! HTML::linkAction($controller . '@getIndex', 'Cancel', [], ['class' => 'btn btn-warning']) !!}
{!! Form::close() !!}
