@extends($master)
@section('content')
    <h4>Create new setting</h4>
    @include('smorken/settings::setting._form', ['create' => true])
@stop
