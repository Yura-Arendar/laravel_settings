@extends($master)
@section('content')
    <h4>Settings</h4>
    {!! HTML::tableView(
        $models,
        [
            'id' => 'ID',
            'key' => 'Key',
            'descr' => 'Description',
            'value' => 'Value',
        ],
        ['class' => 'table-striped'],
        $controller
    ) !!}
@stop
