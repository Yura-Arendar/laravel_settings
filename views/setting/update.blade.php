@extends($master)
@section('content')
    <h4>Update setting #{{$model->id}}</h4>
    <h5>{{ $model->descr }} [{{ $model->key }}]</h5>
    @include('smorken/settings::setting._form')
@stop
