@extends($master)
@section('content')
    <h4>Setting #{{ $model->id }}</h4>
    {!! HTML::detailView($model, [
        'id' => 'ID',
        'key' => 'Key',
        'descr' => 'Description',
        'value' => 'Value',
    ]) !!}
@stop
